# FosuForum

#### 介绍
佛大校园论坛

#### 软件架构

本项目采用基于BootStrap + Jsp + Servlet + MySQL+ Tomcat的开发模式和Maven构建工具进行佛大校园论坛的制作。

实现的功能包括：用户（或管理员）登录，发表新的帖子，查看帖子，回复帖子（也叫跟帖）以及管理员删除发言不当帖子或着跟帖，首页轮播图，分页查询和模糊搜索功能，管理员实现用户管理，数据报表展示，以及佛大周边交通模块。

#### 效果图
    
主界面 

![home.png](https://gitee.com/Datawing/FosuForum/blob/master/assets/images/home.png)

用户登录 

![login.png](https://gitee.com/Datawing/FosuForum/blob/master/assets/images/login.png)

用户注册 

![register.png](https://gitee.com/Datawing/FosuForum/blob/master/assets/images/register.png)

查看个人信息 

![individualsInfo.png](https://gitee.com/Datawing/FosuForum/blob/master/assets/images/individualsInfo.png)

修改个人信息 

![updateinfo.png](https://gitee.com/Datawing/FosuForum/blob/master/assets/images/updateinfo.png)

用户浏览所有普通帖子 

![view1.png](https://gitee.com/Datawing/FosuForum/blob/master/assets/images/view1.png)

用户浏览所有学习专帖 

![study.png](https://gitee.com/Datawing/FosuForum/blob/master/assets/images/study.png)

用户发表普通帖子 

![release1.png](https://gitee.com/Datawing/FosuForum/blob/master/assets/images/release1.png)

用户发表学习专帖 

![release2.png](https://gitee.com/Datawing/FosuForum/blob/master/assets/images/release2.png)

用户删除回帖 

![deleteReply1.png](https://gitee.com/Datawing/FosuForum/blob/master/assets/images/deleteReply1.png)

用户删除学习专区回帖 

![deleteReply2.png](https://gitee.com/Datawing/FosuForum/blob/master/assets/images/deleteReply2.png)

分页和模糊搜索

![pageForum.png](https://gitee.com/Datawing/FosuForum/blob/master/assets/images/pageForum.png)

个人信息 

![individualsInfo.png](https://gitee.com/Datawing/FosuForum/blob/master/assets/images/individualsInfo.png)

管理员管理用户 

![adminUser.png](https://gitee.com/Datawing/FosuForum/blob/master/assets/images/adminUser.png)

管理员删帖 

![po6.png](https://gitee.com/Datawing/FosuForum/blob/master/assets/images/po6.png)

数据报表1——发帖统计 

![po1.png](https://gitee.com/Datawing/FosuForum/blob/master/assets/images/po1.png)

数据报表2——回帖统计 

![po2.png](https://gitee.com/Datawing/FosuForum/blob/master/assets/images/po2.png)

数据报表3——学习专区发帖统计 

![po3.png](https://gitee.com/Datawing/FosuForum/blob/master/assets/images/po3.png)

数据报表4——学习专区回帖统计 

![po4.png](https://gitee.com/Datawing/FosuForum/blob/master/assets/images/po4.png)

数据报表5——管理员统计普通帖子发表数量Top5的普通用户 

![po5.png](https://gitee.com/Datawing/FosuForum/blob/master/assets/images/po5.png)

数据报表6——管理员统计学习专区发表帖子数量Top5的普通用户 

![po6.png](https://gitee.com/Datawing/FosuForum/blob/master/assets/images/po6.png)

佛大周边主界面 

![hobby1.png](https://gitee.com/Datawing/FosuForum/blob/master/assets/images/hobby1.png)

北门交通信息 

![hobby2.png](https://gitee.com/Datawing/FosuForum/blob/master/assets/images/hobby2.png)

东南门交通信息 

![hobby3.png](https://gitee.com/Datawing/FosuForum/blob/master/assets/images/hobby3.png)

中门交通信息 

![hobby4.png](https://gitee.com/Datawing/FosuForum/blob/master/assets/images/hobby4.png)